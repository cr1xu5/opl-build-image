# Cargo chef is not yet working for the whole opl-data project due to
# https://github.com/LukeMathWalker/cargo-chef/issues/4
# Therefore, we do not run `cargo chef cook` with `--release`.
#
# https://github.com/LukeMathWalker/cargo-chef

FROM lukemathwalker/cargo-chef:latest-rust-1-slim as chef

# Download opl-data git repository
WORKDIR /builds/cr1xu5
RUN apt-get update -qq && apt-get install -y git
RUN git clone --depth 1 --branch main https://gitlab.com/openpowerlifting/opl-data.git

# Build dependencies
WORKDIR /builds/cr1xu5/opl-data
RUN cargo chef prepare --recipe-path recipe.json
RUN cargo chef cook --recipe-path recipe.json
RUN cargo chef cook --release --recipe-path recipe.json

# Create image for building in CI
FROM rust:1-slim AS opl-ci
RUN apt-get update -qq && apt-get install -y make
WORKDIR /builds/cr1xu5/opl-data
COPY --from=chef /builds/cr1xu5/opl-data/target/ target/
