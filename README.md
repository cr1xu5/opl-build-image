# Docker image for opl-data CI

Nightly build Docker image for use in [the OpenPowerlifting opl-data repository](https://gitlab.com/openpowerlifting/opl-data).
